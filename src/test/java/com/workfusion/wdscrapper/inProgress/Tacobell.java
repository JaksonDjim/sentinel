package com.workfusion.wdscrapper.inProgress;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Tacobell extends BaseTest {
    private static final String BASE_URL = "https://tacobell.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    public List<Entity> startTask(Input input) {
        try {
            log.info("Starting extraction for " + input);
            List<String> inputZips = Arrays.asList("20002", "10045", "20001", "20006", "10011", "12345", "10010", "20010", "20008") /*input.getZipCodes()*/;
            List<Entity> entities = new ArrayList<>();
            getDriver().get(BASE_URL + "/locations");
            for (int i = 0; i < inputZips.size(); i++) {
                search(inputZips.get(i));

                /*if (isPresent(showAllToggleLink, 0))
                    click(showAllToggleLink, 0);
                expandHours();
                entities.addAll(getPageEntities(entityLink));
                log.info("Entities found: " + entities.size() + "\tZip: " + (i + 1) + "/" + inputZips.size());*/
            }
            return entities;
        } finally {
            tearDown();
        }
    }

    private boolean search(String text) {
        By searchFieldBy = By.xpath("//input[contains(@class,'location-search-input')]");
        if (isPresent(searchFieldBy, Conf.TIMEOUT)) {
            long startTime = CommonUtils.getSeconds();
            boolean isHintVisible;
            do {
                getDriver().findElement(searchFieldBy).clear();
                getDriver().findElement(searchFieldBy).sendKeys(text);
                click(By.xpath("//input[@type='submit']"), 0);
                isHintVisible = isVisible(By.xpath("//*[@class='hint zip']"), 0);
                if (isHintVisible)
                    getDriver().navigate().refresh();
                else {
                    CommonUtils.sleep(1);
                    long start = CommonUtils.getSeconds();
                    boolean isInvisible = isInvisible(By.xpath("//*[@id='js-loader-wrapper'][@class='active']"), Conf.TIMEOUT);
                    System.out.println("Took: " + (CommonUtils.getSeconds() - start) + " ******************* " + isInvisible);
                }
            } while (isHintVisible && CommonUtils.getSeconds() - startTime < Conf.TIMEOUT);
            return true;
        }
        return false;
    }

    private void waitWorking() {
        long startTime = CommonUtils.getSeconds();
        boolean isWorking;
        do {
            isWorking = isInvisible(By.xpath("//*[@id='js-loader-wrapper'][@class='active']"), 0);
            CommonUtils.sleep(2);
        } while (isWorking && CommonUtils.getSeconds() - startTime < Conf.TIMEOUT);
    }
}
