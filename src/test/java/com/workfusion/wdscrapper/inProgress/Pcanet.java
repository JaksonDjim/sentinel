package com.workfusion.wdscrapper.inProgress;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pcanet extends BaseTest {

    private static final String BASE_URL = "http://www.pcaac.org";
    private static final String BASE_XPATH = ".//*[@id=\"mapDiv\"]/div/div[1]/div[3]/div[4]/div/div[2]";
    private static final String INFOBOX_XPATH = ".//*[@id='mapDiv']/div/div[1]/div[3]/div[4]/div";

    private final String CHURCH_NAME = "church name:";
    private final String CHURCH_ADDRESS = "church address:";
    private final String CHURCH_EMAIL = "e-mail:";

    private final int WAIT_ELEMENT_TIMEOUT = 60; // seconds

    private String titlePage = "PCA Church Directory | Presbyterian Church in America: Administrative Committee";

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    protected List<Entity> startTask(Input input) {

/*
        List<Entity> entityList = Lists.newCopyOnWriteArrayList(); // for multithreaded execution
*/
        List<Entity> entityList = Lists.newLinkedList(); //        for single tread execution

        input.getZipCodes().stream().forEach(zipCode -> {
/*
        input.getZipCodes().parallelStream().forEach(zipCode -> {
*/
            int counter = 0;
            final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
            while (true) { // repeating until we get an element
                try {
                    openStoreLocatorPage();
                    waitForLoad(getDriver());
                    if (getDriver().getTitle().equals(titlePage)) {
                        getDriver().switchTo().frame(0);
                        WebElement element = getDriver().findElement(By.xpath("//*[@id='addressBox']"));
                        element.sendKeys(zipCode);
                        element.submit();
                        isPresent(By.xpath(INFOBOX_XPATH), 10);
                        if (waitElement(INFOBOX_XPATH) == null) {
                            counter++;
                            if (counter == 5) {
                                System.out.println("No such element with zipcode: " + zipCode);
                                break;
                            }
                            continue;
                        } else {
                            break;
                        }
                    } else {
                        System.out.println("This page no opened");
                        openStoreLocatorPage();
                    }
                } catch (Exception e) {
                    log.error(e.getStackTrace());
                    continue;
                }
            }

            Map<String, String> church = new HashMap<>();

            List<WebElement> churchAttributes = getElementsByXpath(BASE_XPATH + "/*");// get children

            for (int i = 0, divNum = 1; i < churchAttributes.size(); i++) {
                WebElement parent = churchAttributes.get(i);
                String tag = parent.getTagName();
                String tagNum = tag.equals("div") ? "[" + divNum + "]" : "";
                List<WebElement> children = getElementsByXpath(BASE_XPATH + "/" + tag + tagNum + "/*");

                switch (tag) {
                    case "div":
                        WebElement span = null, a = null;

                        for (WebElement child : children) {
                            if (child.getTagName().equals("span")) {
                                span = child;
                            } else if (child.getTagName().equals("a")) {
                                a = child;
                            }
                        }

                        if (span != null && a == null) { // Church attributes
                            String attr = span.getText().toLowerCase();
                            String value = parent.getText();

                            String[] attribute = value.split(": ");
                            if (attribute.length == 2) {
                                church.put(attribute[0], attribute[1]);
                                if (attr.contains("phone:")) {
                                    entity.setPhone(attribute[1]);
                                }
                            } else {
                                System.err.println(zipCode + " - " + attr + " - " + value);
                            }
                        } else if (span == null && a != null) { // Church address
                            String address = a.getText().replace("\n", " ");
                            church.put(CHURCH_ADDRESS, address);
                            entity.setAddress(address);
                        } else if (span != null && a != null) { // Email
                            String attr = span.getText().toLowerCase();
                            String value = a.getText();
                            entity.setCompanyEmailAddress(value);
                            if (attr.contains(CHURCH_EMAIL)) {
                                church.put(CHURCH_EMAIL, value);
                            }
                        }

                        divNum++; //change div number
                        break;
                    case "h5": // Church name
                        WebElement h5 = null;
                        for (WebElement child : children) {
                            if (child.getTagName().equals("a")) {
                                h5 = child;
                            }
                        }
                        assert h5 != null;
                        String name = h5.getText();
                        church.put(CHURCH_NAME, name);
                        entity.setPrimaryContactName(name);
                        break;
                }
            }


            entityList.add(entity);
            System.out.println("Zip: " + zipCode + " - " + church);
        });
        return entityList;
    }

    protected void waitForLoad(WebDriver driver) {
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    protected WebElement waitElement(String xpath) {
        WebDriverWait wait = new WebDriverWait(getDriver(), WAIT_ELEMENT_TIMEOUT);
        WebElement element = null;
        if (xpath != null) {
            element = wait.until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        }
        return element;
    }

    @Override
    protected void openStoreLocatorPage() {
        getDriver().get(getBaseUrl() + "/church-search");
    }

    @Override
    public Collection<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
