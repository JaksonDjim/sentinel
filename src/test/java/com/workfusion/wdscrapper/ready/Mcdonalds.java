package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Mcdonalds extends BaseTest {

    final String BASE_URL = "http://www.mcdonalds.com";
    final By fullAddress = By.xpath("//ul[@class = 'List HomeHeader-list HomeHeader-list--contact']/li[1]");
    final By phoneAddress = By.xpath("//ul[@class = 'List HomeHeader-list HomeHeader-list--contact']/li[2]");
    final By operationHours = By.xpath("(//span[@class = 'HomeHeader-schedule Schedule'])[2]");
    final By nextButton = By.xpath("//a[@class = 'search_now_btn']");

    @Override
    public Collection<String> collectLinks(Input input) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 3);
        getDriver().get("http://www.mcdonalds.com/us/en/restaurant_locator");
        getDriver().findElement(By.xpath("//a[text() = 'X']")).click();
        getDriver().findElement(By.id("cityZip")).clear();
        getDriver().findElement(By.id("cityZip")).sendKeys("00501");
        getDriver().findElement(By.xpath("//a[@class = 'search_submit_bttn en_search_now']")).click();

        Set<String> links = new HashSet<>();
        getDriver().switchTo().frame(getDriver().findElement(By.xpath("//iframe[@id = 'frameiframegenericpagecontent']")));

        for (String zipcode : input.getZipCodes()) {
            int i = 0;
            getDriver().findElement(By.id("address")).sendKeys(zipcode);
            try {
                wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(nextButton)));
                getDriver().findElement(nextButton).click();
                do {
                    List<String> pageLinks = getUrls(By.xpath("//*[@id='results']/table/tbody/tr/td[2]/a"), Conf.TIMEOUT);
                    links.addAll(pageLinks);
                    getDriver().findElement(By.xpath("//img[@src = 'images/backgrounds/NextButton.png']")).click();
                    i++;
                } while (i < 4);
                List<String> pageLinks = getUrls(By.xpath("//*[@id='results']/table/tbody/tr/td[2]/a"), Conf.TIMEOUT);
                links.addAll(pageLinks);
                log.info("Links found" + links);
            } catch (Exception e) {
                log.error("No links on " + zipcode);
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        Entity entity = new Entity(getWebsiteUrl(), link);
        try {
            getDriver().get(link);
            entity.setBusinessName("MC DONALD'S");
            entity.setAddress(getElementText(this.fullAddress, 2));
            entity.setPhone(getElementText(phoneAddress, 2));
            entity.setCompanyWebsite(link);
            entity.setOperationHours(getElementText(operationHours, 2));
            entity.setFacebookURL("https://www.facebook.com/McDonalds");
            entity.setInstagramURL("https://www.instagram.com/mcdonalds/");
            entity.setTwitterURL("https://twitter.com/McDonalds");
            entity.setYoutubeURL("https://www.youtube.com/user/McDonaldsUS");
            entity.setTumblrURL("http://mcdonalds.tumblr.com/");
            if (getDriver().findElements(By.xpath("//span[text() = 'Free Wifi']")).size() != 0) {
                entity.setWifi("Y");
            } else {
                entity.setWifi("N");
            }
        } catch (Exception e) {
            log.error("McDonalds extraction exception: ", e);
        }
        return Lists.newArrayList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
