package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;


/**
 * Created by Vladimir Shvets on 12/3/2015.
 * //
 */
public class Lordandtaylor extends BaseTest {


    final String BASE_URL = "http://lordandtaylor.com";


    @Override
    public Collection<String> collectLinks(Input input) {
        List<String> links = new ArrayList<>();
        int i = 1;
        while (i < 56) {
            links.add("http://www.lordandtaylor.com/webapp/wcs/stores/servlet/StoreLocatorDetailDisplayView?tab=events&storeId=10151" +
                    "&langId=-3&catalogId=10102&physicalStoreId=" + i + "&distance=0.9436164241569909&uom=miles");
            i++;
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        Entity entity = new Entity(getWebsiteUrl(), BASE_URL);
        List<Entity> entities = new ArrayList<>();
        try {


            getDriver().get(link);
            entity.setBusinessName("Lord & Taylor");
            String fullAddress1 = getElementText(By.xpath("//div[@class = 'left']/p[1]"), 1);
            String fullAddress2 = getElementText(By.xpath("//div[@class = 'left']/p[2]"), 1);
            String fullAddress3 = getElementText(By.xpath("//div[@class = 'left']/p[3]"), 1);
            String phone = getElementText(By.xpath("//div[@class = 'left']/p[4]"), 1);
            entity.setFacebookURL("https://www.facebook.com/lordandtaylor");
            entity.setInstagramURL("https://www.instagram.com/lordandtaylor");
            entity.setPinterestURL("https://www.pinterest.com/lordandtaylor");
            entity.setTwitterURL("https://twitter.com/lordandtaylor");
            entity.setYoutubeURL("https://www.youtube.com/user/LordandTaylorStore");
            entity.setCompanyWebsite(link);
            entity.setCreditCardsAccepted("Y");
            if (getDriver().findElements(By.id("bagItems")).size() != 0) {
                entity.setHasEcommerce("Y");
            } else {
                entity.setHasEcommerce("N");
            }
            entity.setAddress(fullAddress1 + fullAddress2 + fullAddress3);
            entity.setPhone(phone);
            int day = 7;
            List<String> hours = new ArrayList<>();
            while (day < 15) {
                String operationHoursLocal = getDriver().findElement(By.xpath("//div[@class = 'mid' and not(contains (text(), '}'))" +
                        "]/div[" + day + "]/span[2]")).getText();
                hours.add(operationHoursLocal);
                day++;
            }

            entity.setOperationHours(hours.toString());
            entities.add(entity);

        } catch (Exception e) {

        }
        return entities;

    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
