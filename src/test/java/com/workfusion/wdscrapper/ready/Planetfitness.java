package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;

/**
 * Created by Vladimir Shvets on 12/9/2015.
 */
public class Planetfitness extends BaseTest {

    final String BASE_URL = "http://www.planetfitness.com";


    @Override
    public Collection<String> collectLinks(Input input) {
        List<String> links = new ArrayList<>();
        for (String state : input.getStates()) {
            getDriver().get("http://www.planetfitness.com/local-clubs");
            getDriver().findElement(By.id("edit-map-address-input")).sendKeys(state);
            getDriver().findElement(By.id("edit-map-submit")).click();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            List<String> pageLinks = getUrls(By.xpath("//a[@class = 'club-site-link']"), 1);
            links.addAll(pageLinks);
            log.info(state + " links " + links.size());
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        Entity entity = new Entity(getWebsiteUrl(), link);
        getDriver().get(link);
        entity.setBusinessName("PLANET FITNESS");
        String address1 = getElementText(By.xpath("(//div[@class = 'field-content'])[2]"), 1);
        String address2 = getElementText(By.xpath("(//span[@class = 'field-content'])[2]"), 1);
        entity.setAddress(address1 + address2);
        entity.setPhone(getElementText(By.xpath("(//span[@class = 'field-content'])[3]"), 2));
        entity.setCompanyWebsite(link);
        if (getDriver().findElements(By.xpath("//*[contains(text(), '24 hours') or contains(text(), '24 Hours')]")).size() != 0) {
            entity.setOperationHours("24/7");
        }
        entity.setFacebookURL("https://www.facebook.com/planetfitness");
        entity.setInstagramURL("https://www.instagram.com/planetfitness");
        entity.setTwitterURL("https://twitter.com/PlanetFitness");
        entity.setYoutubeURL("https://www.youtube.com/user/planetfitnessnh");
        return Lists.newArrayList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}



