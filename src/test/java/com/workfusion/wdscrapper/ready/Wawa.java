package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Wawa extends BaseTest {

    @Override
    protected List<Entity> startTask(Input input) {
        return extractEntities(input);
    }

    private List<Entity> extractEntities(Input input) {
        List<Entity> entities = new ArrayList<>();
        Set<String> storeNums = new HashSet<>();

        openStoreLocatorPage();
        for (String zip : input.getZipCodes()) {
            entities.addAll(retry(zip, (zipCode) -> extractEntities(zip, storeNums)));
        }
        return entities;
    }

    @Override
    protected void openStoreLocatorPage() {
        getDriver().get("https://www.wawa.com/StoreLocator.aspx");
    }

    private List<Entity> extractEntities(String zip, Set<String> storeNums) {
        List<Entity> entities = new ArrayList<>();
        if (isPresent(By.id("pac-input"), 5)) {
            WebElement inputPostalCode = getDriver().findElement(By.id("pac-input"));
            inputPostalCode.clear();
            inputPostalCode.sendKeys(zip);
            click(By.id("locate_submit"), Conf.TIMEOUT);
            WebDriverHelper.waitInvisible(getDriver(), By.id("loader"), Conf.TIMEOUT);

            String searchError = getElementText(By.id("searchError"));
            if (StringUtils.isBlank(searchError) && isVisible(By.id("resultsDiv"), 1)) {
                entities.addAll(extractEntities(storeNums));
                openStoreLocatorPage();
            }
        }
        return entities;
    }

    private List<Entity> extractEntities(Set<String> storeNums) {
        List<Entity> entities = new ArrayList<>();
        boolean next;
        do {
            int itemSize = getElements(By.xpath("//div[@class='half']/p"), 5).size();
            for (int i = 1; i <= itemSize; i++) {
                click(By.xpath("//*[@id='resultlist']/li[" + i + "]/div[2]/a"), 0);
                CommonUtils.sleepMillis(50);
                String storeNum = getElementTextByXpath("//*[@id='resultlist']/li[" + i + "]/div/div/p/span[@class='store-num']");
                if (!storeNums.contains(storeNum)) {
                    storeNums.add(storeNum);
                    entities.add(extractEntity(i));
                }
            }
            next = isVisible(By.linkText("Next"), 1);
            if (next) {
                click(By.linkText("Next"), Conf.TIMEOUT);
            }
        } while (next);

        return entities;
    }

    private Entity extractEntity(int i) {
        Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        entity.setBusinessName("WAWA FOOD MARKET");
        String address = getElementTextByXpath("//*[@id='resultlist']/li[" + i + "]/div/div/p/span[contains(@data-bind, 'address')]");
        String city = getElementTextByXpath("//*[@id='resultlist']/li[" + i + "]/div/div/p/span[contains(@data-bind, 'city')]");
        String state = getElementTextByXpath("//*[@id='resultlist']/li[" + i + "]/div/div/p/span[contains(@data-bind, 'state')]");
        String zip = getElementTextByXpath("//*[@id='resultlist']/li[" + i + "]/div/div/p/span[contains(@data-bind, 'zip')]");
        entity.setAddress(getFullAddress(address, state, city, zip));
        entity.setPhone(getElementTextByXpath("//*[@id='resultlist']/li[" + i + "]/div/div/p/span[contains(@data-bind, 'phone')]"));
        entity.setOperationHours(getElementTextByXpath("(//p[@class='hours-section'])[" + i + "]/span[contains(@data-bind, 'hours')]"));
        entity.setCompanyWebsite(getBaseUrl());

        setCommonData(entity);

        return entity;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(INSTAGRAM_URL, getUrl(By.xpath("//a[contains(@href, 'instagram.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));

        String storeUrl = getUrl(By.xpath("//a[contains(@href, 'store.wawa.com')]"));
        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(!storeUrl.isEmpty()));

        getDriver().get(storeUrl);
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(isVisible(By.cssSelector("ul.credit-cards"), 5)));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return new ArrayList<>();
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return new ArrayList<>();
    }

    @Override
    public String getBaseUrl() {
        return "https://www.wawa.com";
    }
}
