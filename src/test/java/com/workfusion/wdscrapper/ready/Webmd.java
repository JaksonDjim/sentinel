package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;

public class Webmd extends BaseTest {

    public static final String BASE_URL = "http://doctor.webmd.com/find-a-doctor/specialties";

    @Override
    public Collection<String> collectLinks(Input input) {
        List<String> doctorUrls = new ArrayList<>();

        getDriver().get(BASE_URL);
        List<String> specialtyUrls = getUrls(By.xpath("//li[contains(@class,'alpha')]/a"), Conf.TIMEOUT);
        for (String specialtyUrl : specialtyUrls) {
            getDriver().navigate().to(specialtyUrl);
            List<String> statesUrls = getUrls(By.xpath("//div[contains(@class, 'states')]/ul/li/a"), Conf.TIMEOUT);
            for (String stateUrl : statesUrls) {
                getDriver().navigate().to(stateUrl);
                List<String> cityUrls = getUrls(By.xpath("//div[contains(@class, 'top-cities')]/ul/li/a"), Conf.TIMEOUT);
                for (String cityUrl : cityUrls) {
                    getDriver().navigate().to(cityUrl);
                    boolean isNextAvailable;
                    do {
                        doctorUrls.addAll(getUrls(By.xpath("//li[contains(@class, 'result')]/h2/a"), Conf.TIMEOUT));
                        isNextAvailable = getDriver().findElements(By.xpath("//a[contains(@class, 'rsArrowRight')]")).size() > 0;
                        if (isNextAvailable) {
                            click(By.xpath("//a[contains(@class, 'rsArrowRight')]"), Conf.TIMEOUT);
                        }
                    } while (isNextAvailable);
                }
            }
        }

        return doctorUrls;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);
        List<Entity> entities = new ArrayList<>();
        Entity entity = new Entity(getWebsiteUrl(), link);

        String businessName = getElementText(By.xpath("//div[@class='header']/h1/span"), 0);
        entity.setBusinessName(businessName);

        String prefixRegex = "(Dr.?|Sr.?|Mrs.?|Msgr.?|Ms.?|Mr.?)";
        String title = getToken(prefixRegex, businessName);
        entity.setPrimaryContactTitle(title);
        businessName = removeToken(businessName, prefixRegex, title);

        String suffixRegex = "(M.?D.?|D.?O.?|Pharm.?D.?)";
        String suffix = getToken(suffixRegex, businessName);
        entity.setProfessionalSuffix(suffix);
        businessName = removeToken(businessName, suffixRegex, suffix);
        entity.setPrimaryContactName(businessName.replace(",", ""));
        entity.setContactPhoneNumber(getElementText(By.xpath("//ul[@class='contact']/li[@class='telephone']"), 0));

        String address = getElementText(By.xpath("//span[@itemprop='streetaddress']"), 0);
        String state = getElementText(By.xpath("//span[@itemprop='addressRegion']"), 0);
        String city = getElementText(By.xpath("//span[@itemprop='addressLocality']"), 0).replace(",", "");
        String zip = getElementText(By.xpath("//span[@itemprop='postalCode']"), 0);
        entity.setAddress(getFullAddress(address, state, city, zip));
        entity.setPhone(getElementText(By.xpath("//span[@class='practicephone']"), 0));

        String education = getElementText(By.xpath("//div[@class='education']/p"), 0);
        String[] split = education.split("\\; Graduated");
        if (split.length > 0) {
            entity.setMedicalSchoolAttended(split[0].replace("Medical School: ", "").trim());
        }
        if (split.length > 1) {
            entity.setGraduationYear(split[1].trim());
        }

        click(By.linkText("Insurance"), Conf.TIMEOUT);
        entity.setAcceptedInsurances(getElementText(By.xpath("//section[contains(@class, 'insurance')]/ul"), 0));

        click(By.linkText("Office Information"), Conf.TIMEOUT);
        entity.setFax(getElementText("//*[@id='loc1']/p/strong[contains(., 'Fax')]/..", "Fax"));
        entity.setCompanyWebsite(getElementText("//*[@id='loc1']/p/strong[contains(., 'Practice Website')]/..", "Practice Website"));
        entity.setCompanyWebsite(getElementText("//*[@id='loc1']/p/strong[contains(., 'Practice Website')]/..", "Practice Website"));
        entity.setAcceptingNewPatients(getElementText("//*[@id='loc1']/p/strong[contains(., 'Accepting New Patients')]/..", "Accepting New Patients"));
        entity.setMedicareAccepted(getElementText("//*[@id='loc1']/p/strong[contains(., 'Medicare Accepted')]/..", "Medicare Accepted"));
        entity.setMedicareidAccepted(getElementText("//*[@id='loc1']/p/strong[contains(., 'Medicaid Accepted')]/..", "Medicaid Accepted"));
        entity.setHospitalAffiliation(getElementText(By.xpath("//ul[@itemprop = 'hospitalAffiliation']/li"), 0));
        entity.setOperationHours(getElementText(By.xpath("//table[@class='hours']"), 0));
        entity.setLanguagesSpoken(getElementText("//p[strong[contains(text(), 'Languages Spoken in Office')]]", "Languages Spoken in Office"));

        entities.add(entity);
        return entities;
    }

    private String getElementText(String xpathExpr, String tokenToFind) {
        String elementText = getElementText(By.xpath(xpathExpr), 0);
        if (elementText != null) {
            elementText = elementText.replace(tokenToFind + ":", "").trim();
        }
        return elementText;
    }

    @Override
    public String getBaseUrl() {
        return "http://webmd.com";
    }
}
