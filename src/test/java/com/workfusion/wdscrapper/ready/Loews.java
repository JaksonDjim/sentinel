package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;

public class Loews extends BaseTest {

    private static final String BASE_URL = "http://www.loewshotels.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        WebDriver driver = getDriver();
        driver.get(BASE_URL + "/destinations/en");
        try {
            driver.findElement(By.xpath("//button[@id='viewAsList']")).click(); // Show hotels as list instead of map.
        } catch (UnhandledAlertException uae) {
            try {
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("Alert data: " + alertText);
                alert.accept();
            } catch (NoAlertPresentException nape) {
                nape.printStackTrace();
            }
        }
        return getUrls(By.xpath("//*[@id=\"contain\"]/div/div/div[2]/div/div/p/a[1]"), Conf.TIMEOUT);
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        entity.setCompanyWebsite(driver.getCurrentUrl());
        entity.setAddress(driver.findElement(By.xpath("//*[@class=\"address\"]")).getText());
        entity.setPhone(getElementTextByXpath("//*[@id=\"footer\"]/div[1]/div/div[1]/p[2]/span"));
        entity.setBusinessName(getElementTextByXpath("//*[@id=\"mainvisual\"]/div[1]/div[1]/div/div[1]/span/span[1]"));
        entity.setFacebookURL(getLinkByXpath(driver, "//*[@id=\"contain\"]/div/div[5]/div/ul/li[1]/a"));
        entity.setTwitterURL(getLinkByXpath(driver, "//*[@id=\"contain\"]/div/div[5]/div/ul/li[2]/a"));
        entity.setInstagramURL(getLinkByXpath(driver, "//*[@id=\"contain\"]/div/div[5]/div/ul/li[3]/a"));
        entity.setGoogleplusURL(getLinkByXpath(driver, "//*[@id=\"contain\"]/div/div[5]/div/ul/li[4]/a"));
        entity.setLinkedinURL(getLinkByXpath(driver, "//*[@id=\"contain\"]/div/div[5]/div/ul/li[5]/a"));
        entity.setYoutubeURL(getLinkByXpath(driver, "//*[@id=\"contain\"]/div/div[5]/div/ul/li[6]/a"));
        entity.setReservations(String.valueOf(StringUtils.isEmpty(getElementTextByXpath("//*[@id=\"footer\"]/div[1]/div/div[1]/p[2]/a/span"))));
        entity.setWifi(String.valueOf(getElementTextByXpath("//*[@id=\"contain\"]/div/div[1]/div[2]/div/div/div/div[1]/div/div[1]/div[2]/div/ul").contains("Wi-Fi")));
        entity.setHotelsValetParking(String.valueOf(getElementTextByXpath("//*[@id=\"contain\"]/div/div[1]/div[2]/div/div/div/div/div/div[2]/div[2]/div/ul").contains("Valet parking")));
        return Lists.newArrayList(entity);
    }

    private String getLinkByXpath(WebDriver driver, String xpathExpression) {
        try {
            return driver.findElement(By.xpath(xpathExpression)).getAttribute("href");
        } catch (Exception e){
            return StringUtils.EMPTY;
        }
    }
    

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
