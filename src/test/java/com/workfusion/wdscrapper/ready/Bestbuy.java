package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.openqa.selenium.By;

import static java.util.Collections.singletonList;

public class Bestbuy extends BaseTest {

    @Override
    protected List<Entity> startEntitiesExtractionsByLinks(List<Input> inputsByStates) {
        List<Entity> entities = super.startEntitiesExtractionsByLinks(inputsByStates);

        try {
            cacheCaCommonData();
            Collection<String> links = collectCaLinks();
            for (String link : links) {
                entities.addAll(extractCaEntities(link));
            }
        } finally {
            tearDown();
        }
        return entities;
    }

    public Collection<String> collectCaLinks() {
        getDriver().get("https://www-ssl.bestbuy.ca/en-CA/secure/store-locator-view-all-stores.aspx");
        return getUrls(By.cssSelector("a[href ^= '/en-CA/stores/store-locator.aspx?StoreID="), Conf.TIMEOUT);
    }

    public List<Entity> extractCaEntities(String link) {
        getDriver().get(link);
        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName("BEST BUY MOBILE");
        String address = getElementText(By.xpath("//li[@class='store-address']"));
        String addressDetails = getElementText(By.xpath("//li[@class='store-address-details']"));
        entity.setAddress(address + ", " + addressDetails);
        String elementText = getElementText(By.xpath("//div[@class='store-details']/ul/li/span[contains(., 'Phone')]/.."));
        entity.setPhone(elementText.replace("Phone:", "").trim());
        entity.setOperationHours(getElementText(By.xpath("//ul[contains(@class, 'store-hours')]")));
        entity.setCompanyWebsite("http://www.bestbuymobile.ca");

        setCommonData(entity);

        return singletonList(entity);
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> urls = new HashSet<>();
        for (String zip : input.getZipCodes()) {
            getDriver().get("http://www.bestbuy.com/site/store-locator/" + zip);
            WebDriverHelper.waitVisible(getDriver(), By.id("results-section"), 60);
            urls.addAll(getUrls(By.xpath("//a[@class='store-name' and contains(@href, 'http://stores.bestbuy.com')]"), 0));
        }
        return urls;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);

        List<Entity> entities = new ArrayList<>();
        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName("BEST BUY MOBILE");
        entity.setAddress(getElementText(By.id("address")));
        entity.setPhone(getElementText(By.id("telephone")));
        entity.setCompanyWebsite(getBaseUrl());
        entity.setCompanyEmailAddress(getUrl(By.xpath("//a[@class='send-an-email' and contains(@href, 'mailto')]")));
        entity.setOperationHours(getElementText(By.xpath("//table[@class='c-location-hours-details']")));

        setCommonData(entity);

        entities.add(entity);
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get("http://www.bestbuy.com/site/store-locator");
        cacheSocialNetworksData();

        int shoppingCartSize = getDriver().findElements(By.xpath("//a[contains(@href, 'www.bestbuy.com/cart')]")).size();
        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(shoppingCartSize != 0));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
    }

    private void cacheCaCommonData() {
        getDriver().get("http://www.bestbuy.ca/");
        cacheSocialNetworksData();

        int shoppingCartSize = getDriver().findElements(By.xpath("//a[contains(@href, '/order/basket.aspx')]")).size();
        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(shoppingCartSize != 0));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
    }

    private void cacheSocialNetworksData() {
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]")));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.bestbuy.com";
    }
}
