package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Bellacinosgrinders extends BaseTest {

    @Override
    protected List<Entity> startTask(Input input) {
        List<Entity> entities = new ArrayList<>();
        Set<String> addresses = new HashSet<>();

        for (String zip : input.getZipCodes()) {
            entities.addAll(retry(zip, (zipCode) -> extractEntities(zipCode, addresses)));
        }
        return entities;
    }

    private List<Entity> extractEntities(String zip, Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();
        getDriver().get("http://bellacinosgrinders.com/bellacinos-location-search/");
        getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("div.entry>p>iframe")));
        WebDriverHelper.waitVisible(getDriver(), By.id("search"), 5);

        WebElement inputPostalCode = getDriver().findElement(By.id("search"));
        inputPostalCode.clear();
        inputPostalCode.sendKeys(zip);
        click(By.cssSelector("#schbutton>input"), Conf.TIMEOUT);
        WebDriverHelper.waitVisible(getDriver(), By.id("resultswrapper"), 5);

        int itemSize = getElements(By.cssSelector("#resultswrapper>div[id ^= 'res']"), 0).size();
        for (int i = 1; i <= itemSize; i++) {
            String address = getElementTextByXpath("(//div[@class='storeaddress'])[" + i + "]");
            if (!addresses.contains(address)) {
                Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
                entity.setBusinessName("BELLACINO'S PIZZA & GRINDERS");
                entity.setAddress(address);
                entity.setPhone(getElementTextByXpath("(//div[@class='storephone'])[" + i + "]"));
                entity.setFax(getElementTextByXpath("(//div[@class='storemisc3'])[" + i + "]").replace("fax:", "").trim());
                entity.setCompanyWebsite(getBaseUrl());
                entity.setFacebookURL(getCommonDataCache().get(FACEBOOK_URL));

                addresses.add(address);
                entities.add(entity);
            }
        }
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return new ArrayList<>();
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return new ArrayList<>();
    }

    @Override
    public String getBaseUrl() {
        return "http://bellacinosgrinders.com";
    }
}
