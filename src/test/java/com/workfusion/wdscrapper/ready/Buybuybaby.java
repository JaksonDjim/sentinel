package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class Buybuybaby extends BaseTest {

    private static final String BASE_URL = "http://www.buybuybaby.com";

    @Override
    protected List<Entity> startTask(Input input) {
        final WebDriver driver = getDriver();
        final Set<Entity> entities = Sets.newLinkedHashSet();
        for (String zipCode : input.getZipCodes()) {
            log.info("Current zipCode:" + zipCode);
            driver.get(getBaseUrl() + "/store/selfservice/FindStore");
            new Select(getElementByXpath("//*[@id=\"storeLocatorRadiusInput\"]")).selectByValue("100");
            getElementByXpath("//*[@id=\"storeLocatorOmnibarInput\"]").sendKeys(zipCode + Keys.TAB + Keys.TAB);
            try {
                Thread.sleep(7000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            click(By.xpath("//div[@id='FindStoreBTN']/input"), Conf.TIMEOUT);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final List<WebElement> detailsBlocks = getElementsByXpath("//div[@class=\"storeLocatorLocationItem\"]");
            for (int i = 0; i < detailsBlocks.size(); i++) {
                entities.add(extractEntity(i + 1));
            }
        }
        log.info(entities);
        return Lists.newArrayList(entities);
    }

    private void clickByXpath(String xpathExpression) {
        CommonUtils.jse(getDriver()).executeScript("document.evaluate(arguments[0], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();", xpathExpression);
    }


    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    public Entity extractEntity(Integer blockIndex) {
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final String currentBlock = "//div[@class=\"storeLocatorLocationItem\"][" + blockIndex + "]";
        final String businessName = getElementTextByXpath(currentBlock + "/h6");
        final String storeDetailsBlock = getElementTextByXpath(currentBlock + "/address");
        final String[] splitStoreDetailsBlock = storeDetailsBlock.split("\n");
        final String addressStreet = splitStoreDetailsBlock[0].trim() + ", " + splitStoreDetailsBlock[1].trim();
        final String phoneNumber = splitStoreDetailsBlock[2].trim();
        final String hours = getElementTextByXpath(currentBlock + "/div").replace("\n", ", ");
        final String fb = "https://www.facebook.com/buybuyBABY";
        final String twitter = "https://twitter.com/buybuybaby";
        final String pinterest = "https://www.pinterest.com/buybuybaby/";
        final String instagram = "https://www.instagram.com/buybuybaby/";
        final String yt = "https://www.youtube.com/user/buybuybaby";
        entity.setHasEcommerce("Y");
        entity.setCreditCardsAccepted("Y");
        entity.setBusinessName(businessName);
        entity.setAddress(addressStreet);
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setPinterestURL(pinterest);
        entity.setYoutubeURL(yt);
        entity.setInstagramURL(instagram);
        entity.setBusinessName(businessName);
        return entity;
    }

    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}