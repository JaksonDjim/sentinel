package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Discounttire extends BaseTest {

    private static final String BASE_URL = "http://www.discounttire.com";

    @Override
    protected List<Input> generateTaskInputs() {
        return Lists.newArrayList(new Input(Lists.newArrayList()));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        final WebDriver driver = getDriver();
        driver.get("http://stores.discounttire.com/");
        final List<String> links = Lists.newArrayList();
        final List<String> stateUrls = getUrls(By.xpath("//ul[@class=\"contentlist\"]/li/a"), Conf.TIMEOUT);
        for (String stateUrl : stateUrls) {
            driver.get(stateUrl);
            final List<String> cityUrls = getUrls(By.xpath("//ul[@class=\"contentlist\"]/li/a"), Conf.TIMEOUT);
            for (String cityUrl : cityUrls) {
                driver.get(cityUrl);
                links.addAll(getUrls(By.xpath("//ul[@class=\"city_contentlist\"]/li/a"), Conf.TIMEOUT));
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final String businessName = getElementTextByXpath("//div[@class=\"content_block\"]/h3");
        final String phoneNumber = getElementTextByXpath("//span[@itemprop=\"telephone\"]");
        final String addressStreet = getElementTextByXpath("//div[@itemprop=\"address\"]").replace("\n", ", ");
        final String faxNumber = getElementTextByXpath("//span[@itemprop=\"faxNumber\"]");
        final String hours = getElementTextByXpath("//div[@class='hours']/div/p").replace("\n", ", ");
        final String fb = "http://www.facebook.com/DiscountTire";
        final String twitter = "http://twitter.com/DiscountTire";
        final String pinterest = "http://www.pinterest.com/tiresdotcom/";
        final String instagram = "http://instagram.com/discount_tire";
        final String gp = "https://plus.google.com/+discounttire/posts";
        entity.setBusinessName(businessName);
        entity.setAddress(addressStreet);
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setFax(faxNumber);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setPinterestURL(pinterest);
        entity.setGoogleplusURL(gp);
        entity.setInstagramURL(instagram);
        entity.setBusinessName(businessName);
        return Lists.newArrayList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }


}