package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.StatesUtils;
import org.openqa.selenium.By;

public class Bringfido extends BaseTest {

    final String BASE_URL = "http://www.bringfido.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        List<String> links = new ArrayList<>();
        for (String state : input.getStates()) {
            String region = StatesUtils.isValidAbbreviation(state) ? StatesUtils.getStateNameFromAbbreviation(state) : state;
            getDriver().get(BASE_URL + "/destination/state/" + region.replace(" ", "_"));
            // City URLs from all pages.
            List<String> cityUrls = getUrls(By.xpath("//*[contains(@id,'results_list')]//*[contains(@id,'destination')]//a[text()='Hotels']"), 10);
            int numberOfCities = cityUrls.size();
            for (int i = 0; i< numberOfCities; i++) {
                getDriver().get(cityUrls.get(i));
                // Hotel Overview URLs from all pages.
                links.addAll(getUrls(By.xpath("//*[contains(@id,'results_list')]//*[contains(@id,'hotel')]//a[text()='Hotel Overview']"), 10));
                log.info("Links found: " + links.size() + ".\tState: " + state + ".\tCity: " + (i + 1) + "/" + numberOfCities);
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);
        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName(getElementText(By.name("overview"), 0));
        String addressContent = getElementText(By.className("address"), 0);
        String address = getTextByRegex(addressContent, ".*\\n.*");
        String addressRow = getTextByRegex(getElementText(By.className("address"), 0), "(?<=\\n).*");
        String state = "", city = "", zip = "";
        if (addressRow != null) {
            state = addressRow.split(",").length > 1 ? addressRow.split(",")[1] : null;
            city = addressRow.split(",").length > 0 ? addressRow.split(",")[0] : null;
            zip = addressRow.split(",").length > 2 ? getTextByRegex(addressRow.split(",")[2], "\\d+") : null;
        }
        entity.setAddress(getFullAddress(address, state, city, zip));
        String phone = addressContent.split("\n").length > 2 ? addressContent.split("\n")[2] : null;
        entity.setPhone(phone);
        entity.setFacebookURL(getUrl(By.xpath("//*[@id='bookmarks']//a[contains(@href,'facebook')]"), 0));
        entity.setGoogleplusURL(getUrl(By.xpath("//*[@id='bookmarks']//a[contains(@href,'google')]"), 0));
        entity.setTwitterURL(getUrl(By.xpath("//*[@id='bookmarks']//a[contains(@href,'twitter')]"), 0));
        entity.setWheelchairAccessible(Boolean.toString(isWheelChairAccessible()));
        entity.setHotelsPetFriendly(Boolean.toString(isPetFriendly()));
        return Lists.newArrayList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    private boolean isWheelChairAccessible() {
        String wheelChairXpath = "//*[@id='content_wide']//*[contains(.,'heelchair acce')]";
        return isVisible(By.xpath(wheelChairXpath), 0);
    }

    private boolean isPetFriendly() {
        String petsXpath = "//*[@id='content_wide']//*[contains(.,'pet friendly') or contains(.,'pet-friendly') or " +
                "contains(.,'Pets accepted') or contains(.,'pets accepted') or contains(.,'PetStay')]";
        return isVisible(By.xpath(petsXpath), 0);
    }
}
