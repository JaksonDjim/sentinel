package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.WebDriver;


public class Apeainthepod extends BaseTest {

    private static final String BASE_URL = "http://www.apeainthepod.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        final WebDriver driver = getDriver();
        driver.get(getBaseUrl() + "/maternity-stores.asp");
        List<String> links = Lists.newArrayList();
        for (String state : input.getStates()) {
            try {
                links.add(getElementByXpath("//div[@class=\"statetitle\"]/a[contains(.,\"(" + state + ")\")]").getAttribute("href"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return links;

    }

    @Override
    public Set<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        Set<Entity> result = Sets.newLinkedHashSet();
        for (int i = 0; i < getElementsByXpath("//div[@class=\"SlistCol\"]").size(); i++) {
            try {
                String elementTextByXpath = getElementTextByXpath("//div[@class=\"SlistCol\"][" + (i + 1) + "]/span");
                String[] split = elementTextByXpath.split("\n");
                Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
                if (split.length >= 4) {
                    entity.setBusinessName(split[0].trim() + ", " + split[1].trim());
                    entity.setAddress(split[2].trim() + ", " + split[3].trim());
                }
                if (split.length >= 5) {
                    entity.setPhone(split[4].trim());
                }
                entity.setCreditCardsAccepted("Y");
                entity.setHasEcommerce("Y");
                entity.setOperationHours("-");
                entity.setFacebookURL("https://www.facebook.com/apeainthepod");
                entity.setInstagramURL("https://www.instagram.com/apeainthepodmaternity/");
                entity.setTwitterURL("https://twitter.com/PeaMaternity");
                result.add(entity);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

}