package com.workfusion.wdscrapper.location.provider;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;

/**
 * Created by amorozov
 * Date: 11/23/2015
 */
public class LocationProvider {
    private final Log log = LogFactory.getLog(getClass());

    private static LocationProvider locationProvider;

    private static final String CA_US_ZIP_FILE = "CA_US_ZIP_CODES.csv";
    private static final Collection<LocationEntity> locations = newArrayList();
    private static final Function<LocationEntity, String> GET_ZIP_CODE_FUNC = LocationEntity::getZipCode;
    private static final Ordering<String> SORTER_CASE_INSENSITIVE = Ordering.from(String.CASE_INSENSITIVE_ORDER);

    private LocationProvider() {
        this.loadLocations();
    }

    public static synchronized LocationProvider getLocationProvider() {
        if (locationProvider == null) {
            locationProvider = new LocationProvider();
        }
        return locationProvider;
    }

    public List<String> getZipCodesByState(String state) {
        return getZipCodes(location -> state.equals(location.getState()));
    }

    public List<String> getZipCodesByStates(List<String> states) {
        return getZipCodes(location -> states.contains(location.getState()));
    }

    public List<String> getZipCodesByCountry(String country) {
        return getZipCodes(location -> country.equals(location.getCountry()));
    }

    public List<String> getZipCodesByCountries(List<String> countries) {
        return getZipCodes(location -> countries.contains(location.getCountry()));
    }

    private List<String> getZipCodes(Predicate<LocationEntity> predicate){
        final Collection<LocationEntity> filteredLocations = filter(locations, predicate);
        final List<String> zipCodes = transform(Lists.newArrayList(filteredLocations), GET_ZIP_CODE_FUNC);
        return SORTER_CASE_INSENSITIVE.sortedCopy(zipCodes);
    }

    public Collection<LocationEntity> getLocations(Predicate<LocationEntity> predicate){
        return filter(locations, predicate);
    }

    private void loadLocations() {
        try {
            final URL resource = getResource();
            final CSVReader reader = new CSVReader(new FileReader(resource.getPath()));
            String[] row;
            // skip headers
            reader.readNext();
            while ((row = reader.readNext()) != null) {
                final String zipCode = row[0];
                final String country = row[1];
                final String state = row[2];
                final String city = row[3];
                locations.add(new LocationEntity(zipCode, country, state, city));
            }
            log.info("Number of loaded zips: " + locations.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private URL getResource() {
        return this.getClass().getClassLoader().getResource(CA_US_ZIP_FILE);
    }
}
