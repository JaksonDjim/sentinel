package com.workfusion.wdscrapper.s3;

import java.io.ByteArrayInputStream;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by amorozov
 * Date: 11/24/2015
 */
public class S3PublicPut {

    private static final String ACCESS_KEY = "";
    private static final String BUCKET = "";
    private static final String CONTENT_TYPE = "text/html";
    private static final String PARAM_INLINE = "inline";
    private static final int RETRY_MAX_COUNT = 5;
    private static final String S3_ENDPOINT_URL = "http://s3.amazonaws.com";
    private static final String SECRET_KEY = "";
    private static volatile S3PublicPut instance;
    private final AmazonS3 client;

    private S3PublicPut() {
        client = createS3Client(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY), S3_ENDPOINT_URL);
    }

    public static S3PublicPut getInstance() {
        S3PublicPut localInstance = instance;
        if (localInstance == null) {
            synchronized (S3PublicPut.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new S3PublicPut();
                }
            }
        }
        return localInstance;
    }

    private static AmazonS3 createS3Client(AWSCredentials credentials, String s3EndpointUrl) {
        final AmazonS3Client amazonS3Client;
        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setSignerOverride("S3SignerType");
        amazonS3Client = new AmazonS3Client(credentials, clientConfiguration);
        final S3ClientOptions clientOptions = new S3ClientOptions();
        clientOptions.setPathStyleAccess(true);
        amazonS3Client.setS3ClientOptions(clientOptions);
        amazonS3Client.setEndpoint(s3EndpointUrl);
        return amazonS3Client;
    }

    public String putDataToAmazon(String path, String content) {
        return putDataToAmazonWithRetries(path, content.getBytes(), CONTENT_TYPE, PARAM_INLINE);
    }

    private String putDataToAmazonWithRetries(String path, byte[] content, String contentType, String disposition) {
        for (int i = 1; i <= RETRY_MAX_COUNT; i++) {
            try {
                return putDataToAmazon(BUCKET, path, content, contentType, disposition).getDirectUrl();
            } catch (Exception e) {
                if (i == RETRY_MAX_COUNT) {
                    throw new RuntimeException(e);
                }
            }
        }
        return null;
    }

    private S3ResultItem putDataToAmazon(String bucket, String path, byte[] content, String contentType, String disposition) throws
            Exception {
        final ObjectMetadata metadata = createMetadata(contentType, disposition, (long) content.length);
        executePutOperation(bucket, path, content, client, metadata);
        final S3ResultItem resultItem = new S3ResultItem();
        resultItem.setDirectUrl(constructLinkToFile(bucket, path));
        resultItem.setSignedUrl("Signed url N/A, file is public");
        resultItem.setFilename(path);
        return resultItem;
    }

    private String constructLinkToFile(String bucket, String path) {
        return S3_ENDPOINT_URL + "/" + bucket + "/" + path;
    }

    private void executePutOperation(String bucket, String path, byte[] content, AmazonS3 client, ObjectMetadata metadata) {
        final PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, path, new ByteArrayInputStream(content), metadata);
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        client.putObject(putObjectRequest);
    }

    private ObjectMetadata createMetadata(String contentType, String disposition, Long contentLength) {
        final ObjectMetadata metadata = new ObjectMetadata();
        if (StringUtils.isNotEmpty(contentType)) {
            metadata.setContentType(contentType);
        }
        if (StringUtils.isNotEmpty(disposition)) {
            metadata.setContentDisposition(disposition);
        }
        if (contentLength != null) {
            metadata.setContentLength(contentLength);
        }
        return metadata;
    }
}
