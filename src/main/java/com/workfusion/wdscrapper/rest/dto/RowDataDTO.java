package com.workfusion.wdscrapper.rest.dto;

public class RowDataDTO {

    private String[] headers;

    private String[] values;

    public String[] getHeaders() {
        return headers;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

}
