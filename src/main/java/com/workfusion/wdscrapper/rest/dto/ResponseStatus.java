package com.workfusion.wdscrapper.rest.dto;

public enum ResponseStatus {
    SUCCESS, FAILURE
}
