package com.workfusion.wdscrapper.rest.dto;

/**
 * User: Artyom Strok
 * Date: 16.12.13
 * Time: 21:10
 */
public enum DataStoreColumnType {
    TEXT, INTEGER, DECIMAL, BOOLEAN, DATE, TIMESTAMP
}
